#!/bin/bash
# Dunno why but I had troubles with the line below
# ln -sf /etc/resolv.conf /usr/local/chroots/arch/etc/resolv.conf
cp /etc/resolv.conf /usr/local/chroots/arch/etc/resolv.conf
mount -o bind /dev /usr/local/chroots/arch/dev
if [ -d /usr/local/chroots/arch/home/sadasant/ ]; then
  mount --rbind /home/chronos/user/Downloads/ /usr/local/chroots/arch/home/sadasant/Downloads/
fi
if [ -d /media/removable/USB\ Drive/ ]; then
  if [ ! -d /usr/local/chroots/arch/home/sadasant/drive ]; then
    mkdir /usr/local/chroots/arch/home/sadasant/drive
  fi
  mount --rbind /media/removable/USB\ Drive/ /usr/local/chroots/arch/home/sadasant/drive/
fi
mount -t devpts none /usr/local/chroots/arch/dev/pts
mount -t proc proc /usr/local/chroots/arch/proc
mount -t sysfs sys /usr/local/chroots/arch/sys
chroot /usr/local/chroots/arch /bin/bash
umount /usr/local/chroots/arch/home/sadasant/Downloads/
umount /usr/local/chroots/arch/dev/pts
umount /usr/local/chroots/arch/dev
umount /usr/local/chroots/arch/proc
umount /usr/local/chroots/arch/sys
