﻿# Workstation

Some ideas:

-   Basic Arch Linux.
    -   Packages:
        -   vim
        -   tmux
        -   git
    -   Personalized programs:
        -   dwm
-   Projects workflow:
    -   Make a new virtualization for the project.
        -   Use a linux container.
            -   Docker
            -   OpenVZ
        -   Save the encrypted project image.
            -   [Export a Docker Image to a Tarball](http://tedsdevnotes.wordpress.com/2013/07/31/export-a-docker-image-to-a-tarball/)
        -   Export the image to physical and cloud backups.

Question to answer:

-   What is the size of the image?
-   What are the ways to launch X in the virtualized image?
-   How to share folders between host and hosted OSs?
-   Which font do I want to use?
    -   <https://bbs.archlinux.org/viewtopic.php?pid=555485>
    -   <http://www.reddit.com/r/linux/comments/1acgkt/what_is_your_favourite_console_font/>
    -   <http://www.bok.net/MonteCarlo/>
