# Thoughts

-   [Si una idea es capitalizable, sólo depende de la organización. Si
    una idea no es capitalizable, será incapaz de
    organizarse.](https://twitter.com/sadasant/status/340477625027944448)

-   [Toda forma de poder depende del convencimiento, se alza sobre las
    preconcepciones de sus usuarios, les seduce usando sus
    aspiraciones.](https://twitter.com/sadasant/status/340478817531461632)

-   [Pero siempre será lo mismo: poder, hegemonía, pirámide,
    corporación.](https://twitter.com/sadasant/status/340479347825725440)

-   [Si una sociedad aplaude el intervencionismo y el totalitarismo es
    porque teme respetar y atender al criterio de sus
    iguales.](https://twitter.com/sadasant/status/340482968172777473)

-   [El único criterio para tomar una medida debería ser la
    eficiencia, lo demás es irresponsabilidad y
    cobardía.](https://twitter.com/sadasant/status/340489123498430466)

-   [Motivar la crítica, respetar, prevenir conflictos, mantener el
    carácter, comprometerse, apreciar la diversidad y dedicarse al
    aprendizaje.](https://twitter.com/sadasant/status/340587058617786368)

