﻿Hay una abstracción necesaria en quienes dicen que el conocimiento
humano no es científico, sino tácito, e imposible de expresar. Y es
que la suma de lo que se conoce de algún modo forma parte de un
conocimiento que a larga escala resulta ser más efectivo que al
intentar repetir de manera científica.

Quizás lo expresan de manera primitiva, pero es cierto la acción
individual y de libre interpretación es necesaria para que el
conocimiento total de un grupo sea mayor a la suma de sus partes
porque evita que los conocimientos se solapen del todo.

La acción humana individual es menos capaz de producir resultados
exitosos que la acción humana colectiva: cuando cada individuo o grupo
ejerce su participación independientemente del resto
(de manera creativa).

El individuo puede hacer un gran algoritmo, pero la competencia y/o la
colaboración pueden hacer posibles medios de transporte, Silicon
Valley, Linux, Apple, etc.

El individuo tiene mucho potencial, pero la acción en conjunto tiene
aún más potencial y eso es fácil de probar.

De nada sirve que alguien descubra una fórmula espectacular si no se
pone en práctica, y muy probablemente la pondrán en práctica terceros.

No creo en la individualidad como algo sólido, sino como algo
circunstancial, es decir, la individualidad es irrepetible por el
factor tiempo y espacio en la cual está atada: la cantidad de
variables en el entorno tiempo/espacio son imposibles de replicar,
entonces se hace imposible repetir la individualidad en otro contexto
pero no es algo sólido o crucial para el desarrollo del conocimiento
en general debido a que el individuo puede ser remplazado y sin
embargo conseguir ese conocimiento buscado, es decir la individualidad
es reemplazable por la perspectiva de otros con circunstancias
similares dentro del mismo tiempo-espacio, no daría los mismos
resultados exactos, pero sí podrían tener la misma utilidad (claro, el
resultado a largo plazo puede ser sumamente distinto) la clave para
que la experiencia sea similar a un mayor plazo sería idear un
mecanismo para desechar el residuo mediante incidencias de control
algo como cuando se intenta simular una multiplicación celular
controlada, siempre habrán imperfecciones, sin embargo estas se pueden
descartar para efectos prácticos (no se puede eliminar el ruido sin
hacer ruido) lo interesante de eso es que muy posiblemente entendamos
que la probabilidad de llegar a un desastre apocaliptico incrementan a
medida que decidimos ignorar o tratar de suprimir los efectos de
nuestras acciones quizás porque no supimos desde el principio
harmonizar con el entorno entonces concluiremos que lo mejor será
diseminar vida en otros planetas para que ellos prueben mejor suerte,
y diseñar el mejor modelo de vida que podamos en paralelo, buscar
hacernos *uno* con la naturaleza, es decir: harmonizar con ella
(es decir, existir limpiando el rastro detrás de nosotros).

Supongo que eso sería alcanzar una verdadera superioridad como
existencia.

Quizás el ADN es realmente la tecnología ideal para hacer evolucionar
el conocimiento universal sobre la existencia misma se replica, se
adapta al entorno y guarda registro de las adaptaciones necesarias,
posee bancos de memoria inútiles para la replicación del ADN pero
útiles para escribir cualquier tipo de dato extra eso me hace pensar
si sería útil mandar un diseminador y luego otros mecanismos de
control.

Pero creo que es una pérdida de recursos, si sabes que la probabilidad
de que la vida que resulte de ese ADN logre descifrar el ADN en un
tiempo constante, entonces el producto del ADN sería quien luego
analice los resultados de su propio experimento y en base a eso
perfeccione el diseño.

Lo cual eventualmente llevaría a selección natural entre especies
perfeccionadas para sobrevivir al universo suponiendo como máxima
forma de existencia aquella que intente preservar la existencia actual
y superarla al adentrarse en otras existencias (otros universos?).

Esta sería un mecanismo de control para evitar que un universo deje de
cumplir su función por los efectos de la vida dentro de él mismo, o
simplemente les observaría sin participar pero seguría permitiendo
formas de vida secundarias entrando en una selección natural
interplanetaria.

Creo que en unos miles de años el ser humano será un conjunto de
especies que habrán colonizado el sistema solar y que seguirán
teniendo selección natural entre ellos mismos y que si hay brotes de
existencia superior dentro de esa sopa de especies derivadas del ser
humano, estas formas de existencia superior (en su máxima expresión)
se desvanecerán sin participar en el conflicto ver eso sería hermoso,
la humanidad se habría convertido en un árbol y estaría dando frutos:
formas de existencia superior floreciendo a distintos intervalos y
marchándose para explorar otras realidades muy lejos, como polen o
frutos.
