# Git

## Move backwards in history

    git checkout HEAD~

## Move forward in history

    git checkout HEAD@{1} // Use 2 or 3 to move multiple times.

