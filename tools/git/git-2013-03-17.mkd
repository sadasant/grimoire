# Git

## Check what's up in remote

You can see some details with:

    git remote show origin

But to really see the what's happening there, you need to fetch that
remote first:

    git fetch origin

Then you can log what have changed:

    git log origin/master

Or diff the changes:

    git fetch origin
    git diff origin/master

