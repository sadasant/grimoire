# Git

Remove remote branches that appear locally but were removed before
remotely.

    git fetch -p

