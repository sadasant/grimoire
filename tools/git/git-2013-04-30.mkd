# Git

## git reset --hard commit

`git reset --hard` allows an additional parameter in which we can
determine to which commit we wish to reset.

## Rebasing

`git pull --rebase` makes git fetch, git rebase, update your scripts.

Rebase is at its essence simply takes a series of commits as if they
were individual patches, and applies them at a different point in
history.

It works by going to the common ancestor of the two branches (the one
you’re on and the one you’re rebasing onto), getting the diff
introduced by each commit of the branch you’re on, saving those diffs
to temporary files, resetting the current branch to the same commit as
the branch you are rebasing onto, and finally applying each change in
turn.

Rebasing might be considered a bad practice, one of the reasons is
that timestamps are always preserved. So if you created commit `b`
before they committed `c`, but you didn't push it until later,
rebasing will give you a linear history that's no longer in
chronological order.

Some users don't really care about the chronological order, they say
that the fact with merges is even if you are doing it "right" with
strict topic branches you still can get very hairy unbisectable
conflicts that would be easier to reason about with a rebased history.
Whenever they make a mistake in a commit, they either `--amend` it,
or, if it's an earlier commit, `git rebase --interactive` to fix it.

References:

-   <http://darwinweb.net/articles/the-case-for-git-rebase>
-   <http://git-scm.com/book/ch3-6.html>
-   <http://geekblog.oneandoneis2.org/index.php/2013/04/30/please-stay-away-from-rebase>
-   <https://news.ycombinator.com/item?id=5632220>
-   <https://news.ycombinator.com/item?id=5632266>
-   <https://coderwall.com/p/7aymfa>

## Displaying the topology of git commits

    git log --graph --pretty=oneline --abbrev-commit

Or, to have it with full colors and stuff, add this to your
`.gitconfig`:

    [alias]
        graph = log --graph --color --all --pretty=format:"%C(yellow)%H%C(green)%d%C(reset)%n%x20%cd%n%x20%cn%x20(%ce)%n%x20%s%n"

-   <http://stackoverflow.com/questions/1064361/unable-to-show-a-git-tree-in-terminal>
-   <http://gitready.com/intermediate/2009/01/26/text-based-graph.html>
-   <http://stackoverflow.com/questions/1838873/visualizing-branch-topology-in-git>
-   <http://stackoverflow.com/questions/1057564/pretty-git-branch-graphs>

