## Search and replace in a visual selection

    :'<,'>s/$/./

[More info](http://vim.wikia.com/wiki/Search_and_replace_in_a_visual_selection)

## Buffers

-   Show buffers: `:buffers` or `:ls`
-   Move through buffers: `:b <tab>`
-   Move through buffers by number: `:bN`
-   Or use `:bn` to cycle to the next or `:bp` for previous.
-   Close current buffer: `:bd`
-   Close another bugger:
    -   `:buffers`
    -   `:Nbd` where N is the buffer number.
    -   `:1,3bd` to close a set of buffers.

[More info on buffer switching](vim.wikia.com/wiki/Easier_buffer_switching)
[More info on deleting buffers](http://stackoverflow.com/questions/1269648/how-do-i-close-a-single-buffer-out-of-many-in-vim)


## Jumps

Right from the help (:help jump):

    :ju[mps] Print the jump list (not a motion command).
    {not in Vi} {not available without the |+jumplist| feature}
    
                              *jumplist*
    
    Jumps are remembered in a jump list. With the CTRL-O and CTRL-I
    command you can go to cursor positions before older jumps,
    and back again. Thus you can move up and down the list.
    There is a separate jump list for each window. The maximum
    number of entries is fixed at 100.
    {not available without the |+jumplist| feature}


## I want to count something special.

-   `:%s/.//gn`  : count all chars.
-   `:%s/\t//gn` : count all tabs.

[Reference](http://robert-lujo.com/post/2659036096/name)


## Format lines

-   `:help gq`
-   `gqq`  Format the current line.  With a count format that many lines.
-   `gqap` Format the current paragraph.
-   `gwap` Format the current paragraph and continue where you were.

