# Vim Diaries

Links:
-   [Seven habits of effective text editing, by Bram Moolenaar (2000)](http://www.moolenaar.net/habits.html)
-   [Two decades of productivity: Vim's 20th anniversary](http://arstechnica.com/information-technology/2011/11/two-decades-of-productivity-vims-20th-anniversary/)
-   [Learn Vimscript the Hard Way](http://learnvimscriptthehardway.stevelosh.com/)
-   [Vim + Tmux: How to make Vim your IDE for cross language development](http://talks.nicklamuro.com/vim_tmux)
-   [What are the dark corners of Vim your mom never told you
    about?](http://stackoverflow.com/questions/726894/what-are-the-dark-corners-of-vim-your-mom-never-told-you-about)
