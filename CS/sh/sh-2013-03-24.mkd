# Shell Scripts

## Loop files and directories within the current folder

    for file in *; do
    done

## About loops in bash

You can `continue` and `break` them.  
You can nest them.

## cd into home directory from within a bash script

You should use `/home/$USER`

[Source](http://stackoverflow.com/questions/521226/change-script-directory-to-users-homedir-in-a-shell-script)

## How to make and iterate over associative arrays in bash

    declare -A array
    array[foo]=bar
    array[bar]=foo
    
    for i in "${!array[@]}"
    do
      echo "key  : $i"
      echo "value: ${array[$i]}"
    done

[Source](http://stackoverflow.com/questions/3112687/how-to-iterate-over-associative-array-in-bash)


## Colorizing bash scripts

Just `echo -e "\033[1;34mBOLD BLUE\033[0m"`.

[Source](http://tldp.org/LDP/abs/html/colorizing.html)

