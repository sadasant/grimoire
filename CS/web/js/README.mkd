# JavaScript

## Interesting Links

-   [Testing in Node.js with Mocha](http://brianstoner.com/blog/testing-in-nodejs-with-mocha/)
-   [Using Redis with Node.js](http://www.hacksparrow.com/using-redis-with-node-js.html)
-   [An Application Framework for High Available Systems in Node.JS](http://kth.diva-portal.org/smash/get/diva2:463299/FULLTEXT01.pdf)

