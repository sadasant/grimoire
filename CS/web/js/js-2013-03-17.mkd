# JavaScript

Empezando un proyecto de Node y WebRTC con EscuelaWeb.

## Two sides of WebRTC.

There are two sides to WebRTC.

1.  JavaScript APIs (getUserMedia) that allow an app to access camera
    and microphone hardware. You can use this access to simply display
    the stream locally (perhaps applying effects), or send the stream
    over the network. You could send the data to your server, or you
    could use...

2.  PeerConnection, an API that allows browsers to establish direct
    peer-to-peer socket connections. You can establish a connection
    directly to someone else's browser and exchange data directly. This
    is very useful for high-bandwidth data like video, where you don't
    want your server to have to deal with relaying large amounts of
    data.

Take a look at the [demos](http://www.webrtc.org/demo) to see both parts of WebRTC in action.

[Reference](http://stackoverflow.com/questions/12739185/webrtc-and-websockets-is-there-a-difference)


## WebRTC is different from WebSockets

WebSockets can be used by WebRTC but they're different.

[More Here](http://www.scriptol.com/ajax/webrtc.php).
