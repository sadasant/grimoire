# Haskell

Useful links:

-   [FPComplete: 3. Pure Functions, Laziness, I/O, and Monads](https://www.fpcomplete.com/school/basics-of-haskell/3-pure-functions-laziness-i-o-and-monads)
-   [An HTTP client in Haskell using io-streams](http://news.ycombinator.com/item?id=5330426)
-   [Notions of computation and monads](http://www.disi.unige.it/person/MoggiE/ftp/ic91.pdf)
-   [What I Wish I Knew When Learning Haskell](http://dev.stephendiehl.com/hask/)

