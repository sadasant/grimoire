﻿To play google movies, we need the `google-chrome` package and the
`hal` package from the AUR.

To play netflix movies, you'll need `google-chrome-beta` and
`google-chrome-extension-moonlight`, but to make it work, comment
everything that has something to do with `external_extensions.patch`
in the PKGBUILD and in the install script, then copy:
`/opt/google/chrome-beta/default_apps/external_extensions.json`
as:
`/opt/google/chrome-beta/default_apps/external_extensions.json.bak`,
then add the following lines under the `docs.crx` block:

       "ldjmcjaammmjjilbjpacphekcgfnmdlk" : {
         "external_crx": "novell-moonlight-x86_64.crx",
         "external_version": "3.99.0.3"
       }

If there's another app under `docs.crx`, remember to add the comma in
the closing bracket `}` of the code above.

But still, it doesn't work... because NPAPI plugins not supported (?)

---

Tried to install `netflix-destkop`, to this point, everything is ok,
but I haven't make it run yet, there are missing things for wine, but
`netflix-gecko` said this:

    !IMPORTANT!
    It is vitally important that you install the correct OpenGL drivers for your system!
    All available OpenGL drivers are added as optional dependencies to this package.
    
    Optional dependencies for netflix-desktop
        lib32-ati-dri: for open source ATI driver users
        lib32-catalyst-utils: for AMD Catalyst users
        lib32-intel-dri: for open source Intel driver users [installed]
        lib32-nouveau-dri: for Nouveau users
        lib32-nvidia-utils-bumblebee: for NVIDIA + Bumblebee users
        lib32-nvidia-utils: for NVIDIA proprietary blob users
        xorg-xset: for screensaver/DPMS toggling [installed]

had to install this:

    sudo pacman -S wine_gecko

And now it's working :)
