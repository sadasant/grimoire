# Bash - vi mode

    # Vi keybindings
    set -o vi

This gets you into vi mode.
That mode defaults in _insert_ mode,
you can go to command mode with `esc`.

Resources:

-   [Unix Tricks](http://mmb.pcb.ub.es/~carlesfe/unix/tricks.txt)
-   [bash vi editing mode cheat sheet - post](http://www.catonmat.net/blog/bash-vi-editing-mode-cheat-sheet/)
-   [bash vi editing mode cheat sheet - .txt](http://www.catonmat.net/download/bash-vi-editing-mode-cheat-sheet.txt)

# Ranger

-   [arpinux - fm:ranger](http://arpinux.org/x/doku.php/start:fm:ranger)
-   [ranger as a vim file manager](http://ornicar.github.com/2011/02/12/ranger-as-vim-file-manager.html)
-   [ranger man](http://ranger.nongnu.org/ranger.1.html)


