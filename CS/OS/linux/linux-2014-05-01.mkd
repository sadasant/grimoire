﻿No wifi detected!

Check if the wifi is blocked with:

    rfkill list

If it's _hard-blocked_, use the hardware button (switch) to unblock
it. If it's _soft-blocked_, ue the following command:

    sudo rfkill unblock wifi

This is also useful to block the wifi.. because sometimes is necessary
(?)
