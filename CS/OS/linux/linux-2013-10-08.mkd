﻿# Linux

How to enable 256 color mode on a tty?

    pacman -S fbterm

Then execute `FBTERM=1 fbterm`, and add
`[ -n "$FBTERM"] && export TERM=fbterm`
to the end of ~/.bashrc if the shell is bash.

[Source](https://bbs.archlinux.org/viewtopic.php?pid=987815#p987815)
