# MySQL

## Working with a remote db.

    mysql -u fooUser -p -h 44.55.66.77

Resources:
-   [MySQL - Connect to your database remotely, by RackSpace](http://www.rackspace.com/knowledge_center/article/mysql-connect-to-your-database-remotely).

## Install only the mysql client

You can install only the mysql client searching for the package
`mysql-client` (In Arch: `mysql-clients`).

-   [Is there a way to only install the mysql client (Linux)?](http://stackoverflow.com/questions/5287220/is-there-a-way-to-only-install-the-mysql-client-linux)


## Disable autocompletion for a quicker startup

    mysql -u fooUser -p database -h 44.55.66.77 -A

