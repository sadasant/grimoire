# MySQL Tips

I've been having issues trying to make WebERP work.
Mostly because I'm noob :P

## [mysqlimport: Error: Table `database.mydatabase` doesn't exist](#mysqlimport-exists)
It's usually because you've dumped the database earlier,
**if so**, the answer is to tell mysql to excecute
all the mysql queries in the file:
(Remember to create the database first)

    mysql db_name < database.sql

[Source](http://bookmarks.honewatson.com/2008/05/06/mysqlimport-error-table-databasemydatabase-doesnt-exist/).

