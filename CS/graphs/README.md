# Generating Graphs

To generate _png_ graphs based on _dot_ files:

    dot -Tpng source.dot -o result.png

Examples with images can be found [here](http://www.karakas-online.de/forum/viewtopic.php?t=2647).

To know more, just `dot` the `.dot` file and it will
display how it processes the missing info (all the options).

More about how to mix graphvz tools: [link to strackoverflow](http://stackoverflow.com/questions/8002352/how-to-control-subgraphs-layout-in-dot)

All the attributes are [here](http://www.graphviz.org/doc/info/attrs.html).

Interesting links:

-   [GraphViz Workspace](http://graphviz-dev.appspot.com/)
-   [Google Devs: GraphViz Charts (Experimental)](https://developers.google.com/chart/image/docs/gallery/graphviz)
