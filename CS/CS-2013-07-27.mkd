# Computer Science

>   Randomly inserting nop instructions in programs can easily
    increase performance by 5% or more, and no, compilers cannot
    easily exploit this. It's usually a combination of branch
    predictor and cache behaviour, but it can just as well be e.g. a
    reservation station stall (even in case there are no dependency
    chains that are broken or obvious resource over-subscriptions
    whatsoever).

References:

-   [experiments with optimizing sha256 on a 64-bit intel cpu](https://github.com/tangentstorm/coinops/tree/junkops)
-   [StackOverflow: Why would introducing useless MOV instructions speed up a tight loop in x86_64 assembly?](http://stackoverflow.com/questions/17896714/why-would-introducing-useless-mov-instructions-speed-up-a-tight-loop-in-x86-64-a)
-   [MAO - an Extensible Micro-Architectural Optimizer](http://research.google.com/pubs/pub37077.html)

