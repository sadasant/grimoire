# Types

>   Few terms are used in popular and scientific discourse more
    promiscuously than 'model'. A model is something to be admired and emulated,
    a pattern, a case in point, a type, a prototype, a specimen, a mock-up, a
    mathematical description-- almost anything from a naked blonde to a quadratic
    equation -- and may bear to what it models almost any relation of symbolization.
    (Goodman, 1968, p. 171).

Interesting links

-   [Reddit r types](http://www.reddit.com/r/types)
-   [Unordered tuples and type algebra](http://byorgey.wordpress.com/2012/08/24/unordered-tuples-and-type-algebra/)
-   [The topology of the set of all types](http://math.andrej.com/2012/03/30/the-topology-of-the-set-of-all-types-2/)
-   [The Algebra of Data, and the Calculus of Mutation](http://blog.lab49.com/archives/3011)

