﻿# Go Diaries

Things that I learn while go-ing.

## Go resources

- [Go playground](http://play.golang.org/).
- [Go Source Code](http://tip.golang.org/src/pkg/).
- [Go Language Patterns](https://sites.google.com/site/gopatterns/).

## Papers about Go
- [Analysis of the Go runtime scheduler](http://www.cs.columbia.edu/~aho/cs6998/reports/12-12-11_DeshpandeSponslerWeiss_GO.pdf)

## Blog Posts

- [Applying the Clean Architecture to Go](http://manuel.kiessling.net/2012/09/28/applying-the-clean-architecture-to-go-applications/).
- [RESTful Web Service in Go Powered by the Google App Engine](http://www.drdobbs.com/cloud/restful-web-service-in-go-powered-by-the/240006401).
- [Lexical Scanning in Go - Rob Pike](http://rspace.googlecode.com/hg/slide/lex.html#landing-slide).
- [Go on Heroku](http://mmcgrana.github.com/2012/09/getting-started-with-go-on-heroku.html).
- [Go Memory Model](http://golang.org/ref/mem).
- [Network Programming in Go](http://jan.newmarch.name/go/).
- [Fastest random string: StackOverflow](http://stackoverflow.com/questions/12771930/what-is-the-fastest-way-to-generate-a-long-random-string-in-go/12810288#12810288)
- [Go Doodle](http://blog.golang.org/2011/12/from-zero-to-go-launching-on-google.html).
- [What's Going On](http://jmoiron.net/blog/whats-going-on/?utm_medium=referral&utm_source=t.co)
- [Profiling go programs](http://blog.golang.org/2011/06/profiling-go-programs.html)
- [Optimizing Real World Go](http://bpowers.github.com/weblog/2013/01/05/optimizing-real-world-go/)
- [Writing type parametric functions in Go](http://blog.burntsushi.net/type-parametric-functions-golang)
- [Statically Recompiling NES Games into Native Executables with LLVM and Go](http://andrewkelley.me/post/jamulator.html)
- [Using Anonymous Field to Extend Struct in Go](http://dennissuratna.com/using-anonymous-field-to-extend-in-go/)
- [How to use interfaces in Go](http://jordanorelli.com/post/32665860244/how-to-use-interfaces-in-go)

## Code Examples

- [Reflection and interfaces](http://play.golang.org/p/2OnUaOKIDw).
- [Structs](http://play.golang.org/p/t492LXROIW).
- [Fastest random string: Play](http://play.golang.org/p/do8-V4W75G).
- [Go YAML](https://launchpad.net/goyaml).
- [juju-core](https://launchpad.net/juju-core).
- [The examples from Tony Hoare's seminal 1978 paper "Communicating sequential processes" implemented in Go.](http://godoc.org/github.com/thomas11/csp#S42_facM).
- [Golang/groupcache](https://github.com/golang/groupcache).
- [Binary trees in Go](http://golang.org/doc/play/tree.go).
- [A good implementation of graphs in Go](https://github.com/cchandler/golang-datastructures/tree/master/graph).

## Best Practices

- [Releasing Open Source](http://nathany.com/os-release/)
- [Twelve Go Best Practices](http://talks.golang.org/2013/bestpractices.slide)
- [Common Mistakes Made with Golang, and Resources to Learn From](http://soryy.com/blog/2014/common-mistakes-with-go-lang/)

# Go + Raspberry

- [Arch Raspberry](http://archlinuxarm.org/platforms/armv6/raspberry-pi).
- [Golang + Raspberry](http://dave.cheney.net/tag/go-golang-raspberrypi).
- [Go ARM](http://code.google.com/p/go-wiki/wiki/GoArm).

# Contributing with Go

- [Issues](https://code.google.com/p/go/issues/list).
- [Howto Contribute](http://golang.org/doc/contribute.html).
- [Most watched](https://github.com/languages/Go/most_watched).
