﻿# Go

## On Reflection

How to assign a string to an interface, more or less like
json.Unmarsal does?

Well we need to use reflection. But before, read this article:

[The Laws of Reflection](http://blog.golang.org/laws-of-reflection)

So, let's see, we'll try to achieve what is intended in the following
[example](http://play.golang.org/p/QleBNgrExy):

```
package main

func main() {
    var s string
    a := func(tmp interface{}) string { tmp = "hello"; return tmp.(string) }(&s);
    println("s:", s)
    println("a:", a)
}
```

Which prints:

```
s: 
a: hello
```

And should print `hello` in both.

How? well, the solution is
[this](http://play.golang.org/p/LFr4jEO702):

```
package main

import (
    "fmt"
    "reflect"
    "errors"
)

func saveString(x interface{}) error {
    t := reflect.TypeOf(x)
    v := reflect.ValueOf(x)

    if v.Kind() == reflect.Ptr {
        if v.IsNil() {
            v.Set(reflect.New(t.Elem()))
        }
    } else {
        return errors.New("Non-pointer argument")
    }

    if t.Elem().Kind() != reflect.String {
        return errors.New("Arg is not a string")
    }

    v.Elem().Set(reflect.ValueOf("Hello, world!"))
    return nil
}

func main() {
    var s string
    if err := saveString(&s); err != nil {
        fmt.Println(err)
    }
    fmt.Println(s)
}
```
