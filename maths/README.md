﻿# Mathematics

## Intuitive quotes:

1.  About the relation between integers, rational numbers and groups:

    > If you want to measure and describe the number of occurrences of
    > something, use an integer. If you want to measure and describe the amount
    > of something, use a rational number. If you want to measure and describe
    > the symmetry of a structure, use a group.

## Good resources:

-   [Intro to Group Theory](http://ureddit.com/class/23794/intro-to-group-theory)
-   [Abstract Algebra: Theory and Applications](http://abstract.ups.edu/)
-   [Algebraic Set Theory](http://www.phil.cmu.edu/projects/ast/)
-   [Harvard Abstract Algebra](http://www.youtube.com/watch?v=XCpW9fY3FkA&list=PLA7B08F1D8252DE29)
-   [Theoretical Computer Science MATH Cheat Sheet](http://www.tug.org/texshowcase/cheat.pdf)
-   [FTP full of math books](ftp://210.45.114.81/­math/)

## Topics

### Topology

-   [Rober Adler's researches about Stochastic Algebraic Topology](http://webee.technion.ac.il/people/adler/research.html#sat)

### Isomorphismes

-   [The First Isomorphism Theorem by Tim Sullivan](http://www.tjsullivan.org.uk/pdf/morphthm2014.pdf)
