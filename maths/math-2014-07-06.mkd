﻿To aproximate rational numbers we can use this: <https://en.wikipedia.org/wiki/Dyadic_rational>

Just binary divide any section:

From 0/1 to 1/1 we have 1/2
From 0/1 to 1/2 we have 1/4
From 0/1 to 1/4 we have 1/8
From 0/1 to 1/8 we have 1/16

From 1/2 to 1/1 we have 3/4
From 3/4 to 1/1 we have 7/8
From 7/8 to 1/1 we have 15/16
