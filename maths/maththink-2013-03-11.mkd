# Math Think

    And  ^  &  Conjunction
     Or  v  |  Disjunction
    Not  ¬  ≠  Negation


Truth Table of And:

    ┌───┬───┬─────┐
    │ ϕ │ ψ │ ϕ^ψ │
    ├───┼───┼─────┤
    │ T │ T │  T  │
    │ T │ F │  F  │
    │ F │ T │  F  │
    │ F │ F │  F  │
    └───┴───┴─────┘


Truth Table of Or:

    ┌───┬───┬─────┐
    │ ϕ │ ψ │ ϕvψ │
    ├───┼───┼─────┤
    │ T │ T │  T  │
    │ T │ F │  T  │
    │ F │ T │  T  │
    │ F │ F │  F  │
    └───┴───┴─────┘


Truth Table of Not:

    ┌───┬─────┐
    │ ϕ │ ϕ¬ψ │
    ├───┼─────┤
    │ T │  F  │
    │ F │  T  │
    └───┴─────┘

