# Mathematical Thinking

## Equivalence

The statements ϕ and Ψ are said to be equivalent if each implies the
other.

Biconditional `⇔` is an abbreviation of `(ϕ ⇒ ψ) ^ (ψ ⇒ ϕ)`.

E. g.: `(ϕ ^ ψ) v (¬ϕ)` is equivalent to `ϕ ⇒ ψ`.

Forms of implication:

-   ϕ implies ψ
-   If ϕ then ψ
-   ϕ is sufficient for ψ
-   ϕ only if ψ             (Not the same as "If ψ then ϕ")
-   ψ if ϕ                  (Here the variables are inverted)
-   ψ whenever ϕ
-   ψ is necessary for ϕ

Forms of equivalence:

-   ϕ is equivalent to ψ
-   ϕ is necessary and sufficient for ψ
-   ϕ if and only if ψ (iff)




